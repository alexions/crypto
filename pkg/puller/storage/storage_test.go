package storage

import "testing"

func TestSetGet(t *testing.T) {
	s := New()
	s.Set("test", 1)
	val, ok := s.Get("test")

	if !ok {
		t.Fatal("Cannot get stored value")
	}

	if val.(int) != 1 {
		t.Fatal("Wrong value stored")
	}
}

func TestSetUpdate(t *testing.T) {
	s := New()
	s.Set("test", 1)
	s.Set("test", "new")
	val, ok := s.Get("test")

	if !ok {
		t.Fatal("Cannot get stored value")
	}

	if val.(string) != "new" {
		t.Fatal("Wrong value stored")
	}
}

func TestOnUpdate(t *testing.T) {
	s := New()
	eventFired := false
	// Subscribe to updates
	s.OnUpdate(func(key string, value interface{}) {
		eventFired = true
		if key != "test" || value.(string) != "hello" {
			t.Fatal("wrong key/value pair on update")
		}
	})
	s.Set("test", "hello")
	if !eventFired {
		t.Fatal("OnUpdate did not fire event")
	}
}

func TestToJson(t *testing.T) {
	s := New()
	s.Set("a", 1)
	s.Set("b", "hello")

	var expected string = `{"a":1,"b":"hello"}`
	var actual string = string(s.ToJson())
	if actual != expected {
		t.Fatal("Unexpected json convertion")
	}
}

func TestEmptyGet(t *testing.T) {
	s := New()
	val, ok := s.Get("non-existing")
	if ok || val != nil {
		t.Fatal("Got non-existing value")
	}
}

func TestDelete(t *testing.T) {
	s := New()
	s.Set("aaa", 123)

	val, ok := s.Get("aaa")
	if !ok || val.(int) != 123 {
		t.Fatal("Cannot get stored value")
	}

	s.Delete("aaa")
	val, ok = s.Get("aaa")
	if ok || val != nil {
		t.Fatal("Got deleted value, but should not")
	}
}
