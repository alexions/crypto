package storage

import (
	"encoding/json"
	"sync"
)

type Storage struct {
	hm        map[string]interface{}
	listeners []func(string, interface{})
	rw        sync.RWMutex
}

func New() *Storage {
	return &Storage{
		hm:        make(map[string]interface{}),
		listeners: make([]func(string, interface{}), 0),
	}
}

func (s *Storage) Set(key string, value interface{}) {
	s.rw.Lock()
	defer s.rw.Unlock()

	s.notify(key, value)
	s.hm[key] = value
}

func (s *Storage) Get(key string) (interface{}, bool) {
	s.rw.RLock()
	defer s.rw.RUnlock()

	if val, ok := s.hm[key]; ok {
		return val, ok
	}
	return nil, false
}

func (s *Storage) Delete(key string) {
	s.rw.Lock()
	defer s.rw.Unlock()

	delete(s.hm, key)
}

func (s *Storage) OnUpdate(listener func(key string, value interface{})) {
	// FIXME: Potential race-condition
	s.listeners = append(s.listeners, listener)
}

func (s *Storage) ToJson() []byte {
	s.rw.RLock()
	defer s.rw.RUnlock()
	b, _ := json.Marshal(s.hm)
	return b
}

func (s *Storage) notify(key string, value interface{}) {
	for _, listener := range s.listeners {
		listener(key, value)
	}
}
