package app

import (
	"net/http"
)

func (app *App) handler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	w.Write(app.storage.ToJson())
}
