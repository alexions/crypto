package app

import (
	"net/http"
	"strconv"
	"time"

	"bitbucket.org/alexions/crypto/pkg/logger"
	"bitbucket.org/alexions/crypto/pkg/pubsub"
	"bitbucket.org/alexions/crypto/pkg/puller/storage"
	"github.com/satori-com/satori-rtm-sdk-go/rtm"
)

type App struct {
	puller  Puller
	parser  Parser
	pubsub  pubsub.PubSub
	options Options

	storage   Storager
	rtmClient *rtm.RTMClient
	channels  ControlChannels
}

type Options struct {
	Logger        Logger
	Interval      time.Duration
	ListeningPort int
	Channel       string
}

type ControlChannels struct {
	quitC chan bool
	proxy chan []byte
}

func New(puller Puller, parser Parser, ps pubsub.PubSub, options Options) *App {
	if options.Logger == nil {
		options.Logger = logger.New()
	}
	return &App{
		puller:  puller,
		parser:  parser,
		options: options,
		pubsub:  ps,

		storage: storage.New(),
		channels: ControlChannels{
			quitC: make(chan bool, 1),
			proxy: make(chan []byte, 100),
		},
	}
}

func (app *App) PullAndServe() {
	// TODO: Split this function to be able to test it properly.
	// TODO: Make a graceful shutdown using `mux *http.ServeMux`

	// Connect to Satori PubSub system
	err := app.pubsub.Connect()
	if err != nil {
		app.options.Logger.Fatal(err)
	}

	// We can run loop forever. There is no way to shutdown the app by request.
	// The App can be only killed by OS or can exit on critical error
	go app.parse(app.channels.proxy)

	// Setup infinite puller interval
	go app.pull(app.channels.proxy)

	// Subscribe to storage changes
	app.storage.OnUpdate(func(key string, value interface{}) {
		message := DataItem{
			Key:   key,
			Value: value,
		}
		app.pubsub.Publish(app.options.Channel, message)
	})
	app.serveHTTP()
}

func (app *App) Shutdown() {
	app.options.Logger.Info("Shutting down the application")
	app.channels.quitC <- true
	close(app.channels.proxy)
	app.options.Logger.Info("Good bye!")
}

func (app *App) parse(c <-chan []byte) {
	for data := range c {
		err := app.parser.Parse(data, app.storage)
		if err != nil {
			app.options.Logger.Fatal(err)
		}
	}
}

func (app *App) pull(c chan<- []byte) {
	err := app.puller.Pull(c)
	if err != nil {
		app.options.Logger.Fatal(err)
	}

	for {
		select {
		case <-time.After(app.options.Interval):
			app.options.Logger.Info("Pulling new changes")
			err := app.puller.Pull(c)
			if err != nil {
				app.options.Logger.Fatal(err)
			}
		case <-app.channels.quitC:
			app.options.Logger.Info("Stop pulling new changes")
			return
		}
	}
}

func (app *App) serveHTTP() {
	http.HandleFunc("/", app.handler)
	app.options.Logger.Info("Listening on port:", app.options.ListeningPort)
	app.options.Logger.Fatal(http.ListenAndServe(":"+strconv.Itoa(app.options.ListeningPort), nil))
}
