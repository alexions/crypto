package app

type Storage map[string]interface{}

type Logger interface {
	Info(v ...interface{})
	Warn(v ...interface{})
	Error(e error)
	Fatal(e error)
	Debug(v ...interface{})
}

type Puller interface {
	Pull(c chan<- []byte) error
}

type Parser interface {
	Parse(data []byte, storage Storager) error
}

type Storager interface {
	Get(key string) (interface{}, bool)
	Set(key string, value interface{})
	Delete(key string)
	OnUpdate(func(key string, value interface{}))
	ToJson() []byte
}

type DataItem struct {
	Key   string      `json:"key"`
	Value interface{} `json:"value"`
}
