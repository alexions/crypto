package app

import (
	"testing"
	"time"
)

type testPuller struct {
}

func (p *testPuller) Pull(c chan<- []byte) error {
	c <- []byte("hello")
	return nil
}

type testParser struct {
}

func (p *testParser) Parse(data []byte, storage Storager) error {
	storage.Set("data", data)
	return nil
}

func TestAppPull(t *testing.T) {
	tp := &testPuller{}
	app := New(tp, &fakeParser{}, &fakePubsub{}, Options{
		Interval: 1,
	})
	c := make(chan []byte, 1)

	go app.pull(c)

	select {
	case res := <-c:
		if string(res) != "hello" {
			t.Fatal("Unespected channel content")
		}
	case <-time.After(3 * time.Second):
		t.Fatal("No data in channel")
	}
	app.Shutdown()
}

func TestAppParse(t *testing.T) {
	tp := testParser{}
	app := New(&fakePuller{}, &tp, &fakePubsub{}, Options{})

	c := make(chan []byte)
	go app.parse(c)
	c <- []byte("hello")
	close(c)

	data, ok := app.storage.Get("data")
	if !ok || string(data.([]byte)) != "hello" {
		t.Fatal("Puller did not parse data properly")
	}
}
