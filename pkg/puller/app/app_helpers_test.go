package app

type fakePuller struct{}
type fakeParser struct{}
type fakePubsub struct{}

func (p *fakePuller) Pull(c chan<- []byte) error {
	return nil
}

func (p *fakeParser) Parse(data []byte, storage Storager) error {
	return nil
}

func (ps *fakePubsub) Connect() error {
	return nil
}
func (ps *fakePubsub) Publish(channel string, data interface{}) {
}

func (ps *fakePubsub) Subscribe(channel string) <-chan []byte {
	return make(chan []byte)
}

func testGetApp() *App {
	app := New(&fakePuller{}, &fakeParser{}, &fakePubsub{}, Options{})
	return app
}
