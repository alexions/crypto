package connector

import "net/http"

type Connector struct{}

func (c Connector) Get(url string) (*http.Response, error) {
	return http.Get(url)
}

func New() Connector {
	return Connector{}
}
