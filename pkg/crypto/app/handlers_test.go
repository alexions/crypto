package app

import (
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestFormatterJSON(t *testing.T) {
	app := New(Options{})
	app.storageSet("ABC", &Coin{
		Symbol: "ABC",
		Price:  1,
		Rank:   2,
	})
	app.storageSet("BTC", &Coin{
		Symbol: "BTC",
		Price:  123.45,
		Rank:   1,
	})
	app.storageSet("HOT", &Coin{
		Symbol: "HOT",
		Price:  2,
		Rank:   3,
	})
	coins := app.sortedByRank()

	actual := string(app.formatterJSON(coins))
	expected := `[{"Symbol":"BTC","Price":123.45,"Rank":1},{"Symbol":"ABC","Price":1,"Rank":2},{"Symbol":"HOT","Price":2,"Rank":3}]`

	if actual != expected {
		t.Fatal("Unexpected JSON formatter output. Expected", expected, "Actual:", actual)
	}
}

func TestFormatterCSV(t *testing.T) {
	app := New(Options{})
	app.storageSet("ABC", &Coin{
		Symbol: "ABC",
		Price:  1,
		Rank:   2,
	})
	app.storageSet("BTC", &Coin{
		Symbol: "BTC",
		Price:  123.45,
		Rank:   1,
	})
	app.storageSet("HOT", &Coin{
		Symbol: "HOT",
		Price:  2,
		Rank:   3,
	})
	coins := app.sortedByRank()

	actual := string(app.formatterCSV(coins))
	expected := "Symbol,Price,Rank\nBTC,123.450,1\nABC,1.000,2\nHOT,2.000,3\n"

	if actual != expected {
		t.Fatal("Unexpected JSON formatter output. Expected", expected, "Actual:", actual)
	}
}

func TestHandlerContent(t *testing.T) {
	app := New(Options{})
	app.storageSet("ABC", &Coin{
		Symbol: "ABC",
		Price:  1,
		Rank:   2,
	})
	app.storageSet("BTC", &Coin{
		Symbol: "BTC",
		Price:  123.45,
		Rank:   1,
	})
	app.storageSet("HOT", &Coin{
		Symbol: "HOT",
		Price:  2,
		Rank:   3,
	})

	req, err := http.NewRequest("GET", "/", nil)
	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(app.handler)
	handler.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v", status, http.StatusOK)
	}

	if ctype := rr.Header().Get("Content-Type"); ctype != "application/json" {
		t.Errorf("content type header does not match: got %v want %v", ctype, "application/json")
	}

	expected := `[{"Symbol":"BTC","Price":123.45,"Rank":1},{"Symbol":"ABC","Price":1,"Rank":2},{"Symbol":"HOT","Price":2,"Rank":3}]`
	if rr.Body.String() != expected {
		t.Errorf("handler returned unexpected body: got %v want %v", rr.Body.String(), expected)
	}
}

func TestHandlerLimit(t *testing.T) {
	app := New(Options{})
	app.storageSet("ABC", &Coin{
		Symbol: "ABC",
		Price:  1,
		Rank:   2,
	})
	app.storageSet("BTC", &Coin{
		Symbol: "BTC",
		Price:  123.45,
		Rank:   1,
	})
	app.storageSet("HOT", &Coin{
		Symbol: "HOT",
		Price:  2,
		Rank:   3,
	})

	req, err := http.NewRequest("GET", "/?limit=2", nil)
	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(app.handler)
	handler.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v", status, http.StatusOK)
	}

	expected := `[{"Symbol":"BTC","Price":123.45,"Rank":1},{"Symbol":"ABC","Price":1,"Rank":2}]`
	if rr.Body.String() != expected {
		t.Errorf("handler returned unexpected body: got %v want %v", rr.Body.String(), expected)
	}
}

func TestHandlerCSVFormatter(t *testing.T) {
	app := New(Options{})
	app.storageSet("ABC", &Coin{
		Symbol: "ABC",
		Price:  1,
		Rank:   2,
	})
	app.storageSet("BTC", &Coin{
		Symbol: "BTC",
		Price:  123.45,
		Rank:   1,
	})
	app.storageSet("HOT", &Coin{
		Symbol: "HOT",
		Price:  2,
		Rank:   3,
	})

	req, err := http.NewRequest("GET", "/?format=csv", nil)
	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(app.handler)
	handler.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v", status, http.StatusOK)
	}

	if ctype := rr.Header().Get("Content-Type"); ctype != "text/csv" {
		t.Errorf("content type header does not match: got %v want %v", ctype, "application/json")
	}

	expected := "Symbol,Price,Rank\nBTC,123.450,1\nABC,1.000,2\nHOT,2.000,3\n"
	if rr.Body.String() != expected {
		t.Errorf("handler returned unexpected body: got %v want %v", rr.Body.String(), expected)
	}
}
