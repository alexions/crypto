package app

import (
	"encoding/json"
	"io/ioutil"
	"math"
	"strconv"
)

func (app *App) PullPrices() error {
	res, err := app.connector.Get(app.services.price.Endpoint)
	if err != nil {
		return err
	}
	response, err := ioutil.ReadAll(res.Body)
	res.Body.Close()
	if err != nil {
		return err
	}

	prices := Prices{}
	err = json.Unmarshal(response, &prices)
	if err != nil {
		return err
	}

	for symbol, priceCoin := range prices {
		app.setPrice(symbol, priceCoin.Prices["usd"])
	}

	return nil
}

func (app *App) PullRanks() error {
	res, err := app.connector.Get(app.services.rank.Endpoint)
	if err != nil {
		return err
	}
	response, err := ioutil.ReadAll(res.Body)
	res.Body.Close()
	if err != nil {
		return err
	}
	ranks := Ranks{}
	err = json.Unmarshal(response, &ranks)
	if err != nil {
		return err
	}

	for symbol, rankCoin := range ranks {
		rank, _ := strconv.Atoi(rankCoin.SortOrder)
		app.setRank(symbol, rank)
	}
	return nil
}

func (app *App) subscribe() {
	pricesC := app.pubsub.Subscribe(app.services.price.Channel)
	ranksC := app.pubsub.Subscribe(app.services.rank.Channel)

	go func(pricesC <-chan []byte) {
		for price := range pricesC {
			message := PriceMessage{}
			err := json.Unmarshal(price, &message)
			if err != nil {
				app.logger.Fatal(err)
			}
			app.setPrice(message.Key, message.Value.Prices["usd"])
		}
	}(pricesC)

	go func(ranksC <-chan []byte) {
		for rank := range ranksC {
			message := RankMessage{}
			err := json.Unmarshal(rank, &message)
			if err != nil {
				app.logger.Fatal(err)
			}

			rank, _ := strconv.Atoi(message.Value.SortOrder)
			app.setRank(message.Key, rank)
		}
	}(ranksC)
}

func (app *App) setPrice(key string, price float32) {
	if coin, ok := app.storageGet(key); ok {
		coin.Price = price
	} else {
		app.storageSet(key, &Coin{
			Symbol: key,
			Price:  price,
			Rank:   math.MaxInt32,
		})
	}
}

func (app *App) setRank(key string, rank int) {
	if coin, ok := app.storageGet(key); ok {
		coin.Rank = rank
	} else {
		app.storageSet(key, &Coin{
			Symbol: key,
			Price:  0,
			Rank:   rank,
		})
	}
}
