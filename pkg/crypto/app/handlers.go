package app

import (
	"bytes"
	"encoding/csv"
	"encoding/json"
	"net/http"
	"strconv"
)

func (app *App) handler(w http.ResponseWriter, r *http.Request) {
	limit, _ := strconv.Atoi(r.URL.Query().Get("limit"))
	if limit <= 0 || limit > len(app.storage.s) {
		limit = len(app.storage.s)
	}
	var coins []byte

	// FIXME: sortedByRank is extremily unefficiend function! Need to build another struct of data to store Coins
	switch r.URL.Query().Get("format") {
	case "csv":
		w.Header().Set("Content-Type", "text/csv")
		coins = app.formatterCSV(app.sortedByRank()[:limit])

	default:
		w.Header().Set("Content-Type", "application/json")
		coins = app.formatterJSON(app.sortedByRank()[:limit])
	}
	w.Write(coins)
}

func (app *App) formatterJSON(data []*Coin) []byte {
	coins, _ := json.Marshal(data)
	return coins
}

func (app *App) formatterCSV(data []*Coin) []byte {
	conv := make([][]string, len(data)+1)
	conv[0] = []string{"Symbol", "Price", "Rank"}
	for i, coin := range data {
		conv[i+1] = []string{coin.Symbol, strconv.FormatFloat(float64(coin.Price), 'f', 3, 32), strconv.Itoa(coin.Rank)}
	}

	b := &bytes.Buffer{}
	w := csv.NewWriter(b)
	w.WriteAll(conv)
	return b.Bytes()
}
