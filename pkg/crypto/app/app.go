package app

import (
	"net/http"
	"strconv"
	"time"

	"bitbucket.org/alexions/crypto/pkg/crypto/cache"
	"bitbucket.org/alexions/crypto/pkg/crypto/connector"
	"bitbucket.org/alexions/crypto/pkg/logger"
	"bitbucket.org/alexions/crypto/pkg/pubsub"
)

const (
	FAILED_ATTEMPTS = 5
	FAILED_INTERVAL = 5

	REPULLING_INTERVAL = 60
)

type App struct {
	pubsub    pubsub.PubSub
	port      int
	logger    Logger
	services  services
	storage   Storage
	cacher    *cache.Cacher // FIXME: not used
	connector Connector
}

type Options struct {
	Logger        Logger
	ListeningPort int
	PubSub        pubsub.PubSub
	PriceService  PriceService
	RankService   RankService
}

func New(options Options) *App {
	if options.Logger == nil {
		options.Logger = logger.New()
	}
	return &App{
		pubsub: options.PubSub,
		port:   options.ListeningPort,
		logger: options.Logger,

		services: services{
			price: options.PriceService,
			rank:  options.RankService,
		},

		storage: Storage{
			s: make(map[string]*Coin),
		},
		cacher:    cache.New(),
		connector: connector.New(),
	}
}

func (app *App) Serve() {
	// Connect to Satori PubSub system
	err := app.pubsub.Connect()
	if err != nil {
		app.logger.Fatal(err)
	}
	app.subscribe()

	// Prepare "data" before serving in sync mode
	err = app.PullData([]func() error{
		app.PullPrices,
		app.PullRanks,
	})
	if err != nil {
		app.logger.Fatal(err)
	}

	// Data is ready. Run pull-interval to re-fetch possible missing data
	go func() {
		for {
			err = app.PullData([]func() error{
				app.PullPrices,
				app.PullRanks,
			})
			if err != nil {
				app.logger.Fatal(err)
			}
			time.Sleep(REPULLING_INTERVAL * time.Second)
		}
	}()

	app.serveHTTP()
}

func (app *App) PullData(pullers []func() error) error {
	var err error
	attempts := FAILED_ATTEMPTS
data_receiver:
	for attempts > 0 {
		attempts--
		for _, puller := range pullers {
			err = puller()
			if err != nil {
				app.logger.Error(err)
				time.Sleep(FAILED_INTERVAL * time.Second)
				continue data_receiver
			}
		}
		return nil
	}
	return err
}

func (app *App) serveHTTP() {
	http.HandleFunc("/", app.handler)
	app.logger.Info("Listening on port:", app.port)
	app.logger.Fatal(http.ListenAndServe(":"+strconv.Itoa(app.port), nil))
}
