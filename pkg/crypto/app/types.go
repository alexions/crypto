package app

import (
	"net/http"
	"sync"
)

type Logger interface {
	Info(v ...interface{})
	Warn(v ...interface{})
	Error(e error)
	Fatal(e error)
	Debug(v ...interface{})
}

type Connector interface {
	Get(string) (*http.Response, error)
}

type Storage struct {
	s  map[string]*Coin
	rw sync.RWMutex
}

type Coin struct {
	Symbol string
	Price  float32
	Rank   int
}

type services struct {
	price PriceService
	rank  RankService
}

type PriceService struct {
	Endpoint string
	Channel  string
}

type RankService struct {
	Endpoint string
	Channel  string
}

type Prices map[string]PriceCoin
type Ranks map[string]RankCoin

type PriceCoin struct {
	Symbol string             `json:"symbol"`
	Prices map[string]float32 `json:"prices"`
}

type RankCoin struct {
	Symbol    string `json:"Symbol"`
	SortOrder string `json:"SortOrder"`
}

type PriceMessage struct {
	Key   string    `json:"key"`
	Value PriceCoin `json:"value"`
}

type RankMessage struct {
	Key   string   `json:"key"`
	Value RankCoin `json:"value"`
}
