package app

import "sort"

/*
 * Current storage implementation is awful. Average access complexity is O(n) per each request (!)
 * Need to re-implement storage data type and decrease access complexity to O(n log n) or O(1) by using cache
 * Space complexity is irrelevant in this case
 */

func (app *App) storageSet(key string, value *Coin) {
	app.storage.rw.Lock()
	defer app.storage.rw.Unlock()

	app.storage.s[key] = value
}

func (app *App) storageGet(key string) (*Coin, bool) {
	app.storage.rw.RLock()
	defer app.storage.rw.RUnlock()

	if coin, ok := app.storage.s[key]; ok {
		return coin, true
	}
	return nil, false
}

func (app *App) storageToArray() []*Coin {
	app.storage.rw.RLock()
	defer app.storage.rw.RUnlock()

	coins := make([]*Coin, 0, len(app.storage.s))
	for _, coin := range app.storage.s {
		coins = append(coins, coin)
	}
	return coins
}

// FIXME: sortedByRank is extremily unefficiend function! Need to build another struct of data to store Coins
func (app *App) sortedByRank() []*Coin {
	coins := app.storageToArray()
	sort.Slice(coins, func(i, j int) bool {
		return coins[i].Rank < coins[j].Rank
	})
	return coins
}
