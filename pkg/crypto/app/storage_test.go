package app

import (
	"reflect"
	"testing"
)

func TestStorageSetGet(t *testing.T) {
	app := New(Options{})
	app.storageSet("BTC", &Coin{
		Symbol: "BTC",
		Price:  123.45,
		Rank:   1,
	})

	coin, ok := app.storageGet("BTC")
	if !ok {
		t.Fatal("No stored coin available")
	}

	if coin.Price != 123.45 || coin.Symbol != "BTC" || coin.Rank != 1 {
		t.Fatal("Wrong coin data")
	}
}
func TestStorageNilGet(t *testing.T) {
	app := New(Options{})

	coin, ok := app.storageGet("BTC")
	if ok {
		t.Fatal("Coin was not stored, but got result")
	}

	if coin != nil {
		t.Fatal("Coin was not stored, but got result")
	}
}

func TestStorageToArray(t *testing.T) {
	app := New(Options{})
	app.storageSet("BTC", &Coin{
		Symbol: "BTC",
		Price:  123.45,
		Rank:   1,
	})
	app.storageSet("ABC", &Coin{
		Symbol: "ABC",
		Price:  1,
		Rank:   2,
	})
	coins := app.storageToArray()

	if len(coins) != 2 {
		t.Fatal("Wrong amount of stored coins")
	}

	expected := map[string]bool{"BTC": true, "ABC": true}
	for _, coin := range coins {
		if _, ok := expected[coin.Symbol]; !ok {
			t.Fatal("Unexpected stored coin")
		}
		delete(expected, coin.Symbol)
	}

	if len(expected) != 0 {
		t.Fatal("not enough stored coins")
	}
}

func TestSortedByRank(t *testing.T) {
	app := New(Options{})
	app.storageSet("ABC", &Coin{
		Symbol: "ABC",
		Price:  1,
		Rank:   2,
	})
	app.storageSet("BTC", &Coin{
		Symbol: "BTC",
		Price:  123.45,
		Rank:   1,
	})
	app.storageSet("HOT", &Coin{
		Symbol: "HOT",
		Price:  2,
		Rank:   3,
	})
	coins := app.sortedByRank()

	expected := []*Coin{
		&Coin{
			Symbol: "BTC",
			Price:  123.45,
			Rank:   1,
		},
		&Coin{
			Symbol: "ABC",
			Price:  1,
			Rank:   2,
		},
		&Coin{
			Symbol: "HOT",
			Price:  2,
			Rank:   3,
		},
	}

	for i, coin := range coins {
		if !reflect.DeepEqual(expected[i], coin) {
			t.Fatal("Wrong sorting order. Expected:", expected[i], "Actual:", coin)
		}
	}
}
