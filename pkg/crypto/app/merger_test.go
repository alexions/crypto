package app

import (
	"bytes"
	"io/ioutil"
	"net/http"
	"testing"
)

type FakeConnector struct{}

func (fc FakeConnector) Get(endpoint string) (*http.Response, error) {
	switch endpoint {
	case "http://prices/":
		body := `{"BTC":{"symbol":"BTC","prices":{"usd":6785.5}},"XXX":{"symbol":"XXX","prices":{"usd":0.01}}}`
		response := &http.Response{
			Status:        "200 OK",
			StatusCode:    200,
			Proto:         "HTTP/1.1",
			Body:          ioutil.NopCloser(bytes.NewBufferString(body)),
			ContentLength: int64(len(body)),
		}
		return response, nil
	case "http://ranks/":
		body := `{"BTC":{"Symbol":"BTC","SortOrder":"1"},"XXX":{"Symbol":"XXX","SortOrder":"999"}}`
		response := &http.Response{
			Status:        "200 OK",
			StatusCode:    200,
			Proto:         "HTTP/1.1",
			Body:          ioutil.NopCloser(bytes.NewBufferString(body)),
			ContentLength: int64(len(body)),
		}
		return response, nil
	}

	return &http.Response{}, nil
}

func TestPullPrices(t *testing.T) {
	fc := FakeConnector{}
	app := New(Options{
		PriceService: PriceService{
			Endpoint: "http://prices/",
		},
	})
	app.connector = fc
	app.PullPrices()

	coin, ok := app.storageGet("BTC")
	if !ok || coin.Symbol != "BTC" {
		t.Fatal("Unable to get pulled coin")
	}

	if coin.Price != 6785.5 {
		t.Fatal("Wrong pulled price")
	}
}
func TestPullRank(t *testing.T) {
	fc := FakeConnector{}
	app := New(Options{
		RankService: RankService{
			Endpoint: "http://ranks/",
		},
	})
	app.connector = fc
	app.PullRanks()

	coin, ok := app.storageGet("XXX")
	if !ok || coin.Symbol != "XXX" {
		t.Fatal("Unable to get pulled coin")
	}

	if coin.Rank != 999 {
		t.Fatal("Wrong pulled rank")
	}
}

func TestMerge(t *testing.T) {
	fc := FakeConnector{}
	app := New(Options{
		PriceService: PriceService{
			Endpoint: "http://prices/",
		},
		RankService: RankService{
			Endpoint: "http://ranks/",
		},
	})
	app.connector = fc
	app.PullRanks()

	coin, ok := app.storageGet("XXX")
	if !ok || coin.Symbol != "XXX" {
		t.Fatal("Unable to get pulled coin")
	}

	if coin.Rank != 999 {
		t.Fatal("Wrong pulled rank")
	}
	if coin.Price != 0 {
		t.Fatal("Pulled only ranks, but price is not zero")
	}

	app.PullPrices()
	coin, ok = app.storageGet("XXX")
	if !ok || coin.Symbol != "XXX" {
		t.Fatal("Unable to get merged coin")
	}

	if coin.Rank != 999 {
		t.Fatal("Wrong pulled rank")
	}
	if coin.Price != 0.01 {
		t.Fatal("Merged value mismatched price")
	}
}
