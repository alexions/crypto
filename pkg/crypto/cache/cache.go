package cache

import "sync"

type Cacher struct {
	rw    sync.RWMutex
	cache map[string][]byte
}

func New() *Cacher {
	return &Cacher{
		cache: make(map[string][]byte),
	}
}

func (c *Cacher) SetCache(key string, data []byte) {
	c.rw.Lock()
	defer c.rw.Unlock()

	c.cache[key] = data
}

func (c *Cacher) GetCache(key string) ([]byte, bool) {
	c.rw.RLock()
	defer c.rw.RUnlock()

	data, ok := c.cache[key]
	return data, ok
}

func (c *Cacher) DeleteCache(key string) {
	c.rw.Lock()
	defer c.rw.Unlock()

	delete(c.cache, key)
}
