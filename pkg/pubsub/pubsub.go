package pubsub

type PubSub interface {
	Connect() error
	Publish(channel string, data interface{})
	Subscribe(channel string) <-chan []byte
}
