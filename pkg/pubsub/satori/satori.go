package satori

import (
	"fmt"

	"bitbucket.org/alexions/crypto/pkg/logger"
	"github.com/satori-com/satori-rtm-sdk-go/rtm"
	"github.com/satori-com/satori-rtm-sdk-go/rtm/pdu"
	"github.com/satori-com/satori-rtm-sdk-go/rtm/subscription"
)

type Satori struct {
	Logger logger.Logger

	endpoint string
	appKey   string
	options  rtm.Options
	client   *rtm.RTMClient
}

func New(endpoint string, appKey string, options rtm.Options) *Satori {
	return &Satori{
		Logger: logger.New(),

		endpoint: endpoint,
		appKey:   appKey,
		options:  options,
	}
}

func (s *Satori) Connect() error {
	var err error
	s.client, err = rtm.New(s.endpoint, s.appKey, s.options)
	if err != nil {
		s.Logger.Fatal(err)
		return err
	}

	connected := make(chan bool)
	s.client.OnConnected(func() {
		s.Logger.Info("Connected to Satori RTM!")
		connected <- true
	})
	s.client.OnError(func(err rtm.RTMError) {
		s.Logger.Fatal(err.Reason)
	})

	s.client.Start()
	<-connected
	return nil
}

func (s *Satori) Publish(channel string, message interface{}) {
	s.client.Publish(channel, message)
}

func (s *Satori) Subscribe(channel string) <-chan []byte {
	c := make(chan []byte)
	listener := subscription.Listener{
		OnSubscribed: func(sok pdu.SubscribeOk) {
			fmt.Println("Subscribed to: " + sok.SubscriptionId)
		},
		OnUnsubscribed: func(response pdu.UnsubscribeBodyResponse) {
			fmt.Println("Unsubscribed from: " + response.SubscriptionId)
		},
		OnData: func(data pdu.SubscriptionData) {
			for _, message := range data.Messages {
				c <- message
			}
		},
		OnSubscribeError: func(err pdu.SubscribeError) {
			fmt.Printf("Failed to subscribe %s: %s\n", err.Error, err.Reason)
		},
		OnSubscriptionError: func(err pdu.SubscriptionError) {
			fmt.Printf("Subscription failed. RTM sent the unsolicited error %s: %s\n", err.Error, err.Reason)
		},
	}

	s.client.Subscribe(channel, subscription.SIMPLE, pdu.SubscribeBodyOpts{}, listener)
	return c
}
