// Internal logger system. Uses to change output format and use nanosec when logging time
package logger

import (
	"fmt"
	"os"
	"runtime"
	"time"
)

const (
	WARN  = "[warn] "
	ERR   = "[erro] "
	FATAL = "[fatl] "
	DEBUG = "[debg] "
	INFO  = "[info] "
)

type Logger struct {
}

// New Logger instance
func New() Logger {
	return Logger{}
}

// Logs message with [info] header
func (l Logger) Info(v ...interface{}) {
	fmt.Print(l.getHeader(INFO))
	fmt.Println(v...)
}

// Logs message with [warn] header
func (l Logger) Warn(v ...interface{}) {
	fmt.Print(l.getHeader(WARN))
	fmt.Println(v...)
}

// Logs message with [erro] header
func (l Logger) Error(e error) {
	fmt.Fprintf(os.Stderr, l.getHeader(ERR))
	l.printError(e)
}

// Logs message with [fatl] header
// Fatal is equivalent to logger.Error followed by a call to os.Exit(1).
func (l Logger) Fatal(e error) {
	fmt.Fprintf(os.Stderr, l.getHeader(FATAL))
	l.printError(e)
	os.Exit(1)
}

func (l Logger) printError(e error) {
	fmt.Fprintf(os.Stderr, "%s\n", e)
	if os.Getenv("DEBUG") == "true" {
		buf := make([]byte, 1<<16)
		runtime.Stack(buf, false)
		fmt.Fprintf(os.Stderr, "%s\n", buf)
	}
}

// Logs message with [debg] header
// Will be displayed only when the DEBUG flag is true
func (l Logger) Debug(v ...interface{}) {
	if os.Getenv("DEBUG") == "true" {
		fmt.Print(l.getHeader(DEBUG))
		fmt.Println(v...)
	}
}

// Gets message header by type
func (l Logger) getHeader(prefix string) string {
	t := time.Now()
	return prefix + t.Format("2006/01/02 15:04:05.0000 ")
}
