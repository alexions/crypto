Top Coins Price List App
=========================================================
A price list service prototype for top crypto assets. 

Full documentation can be found in the **[docs](docs/README.md)** folder.


Requirements
=========================================================
- Go: version 1.8
- The Internet connection
- Docker (optional)

Installation
=========================================================
Use a `go-get` tool (or any go-compatible package manager) to download the app:

```
go get bitbucket.org/alexions/crypto/cmd/crypto
```

Or you can use git to clone repo manually:
```
cd $GOPATH
mkdir -p src/bitbucket.org/alexions/crypto
cd src/bitbucket.org/alexions/crypto
git clone git@bitbucket.org:alexions/crypto.git .
```


Usage
=========================================================
The Application batch can be run via `docker-compose`:
```
cd $GOPATH/src/bitbucket.org/alexions/crypto
docker-compose up

// or 
docker-compose run pricePuller
docker-compose run rankPuller
docker-compose run crypto
```

Or you can follow the go-way and build and run each application separately.

After run applications the following endpoints will be available (default):

 - http://localhost:9001 Rank service
 - http://localhost:9002 Price service
 - http://localhost:9003 Crypto App


> Hint: Check `docker-compose.yml` in case if ports already in use.

Check the **[docs](docs/README.md)** to get more info about the Crypto API.

Tests
=========================================================

Testing pipeline is available here: https://bitbucket.org/alexions/crypto/addon/pipelines/home#!/

```
go get ./...
go test ./...
```
