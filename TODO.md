TODO: 
==============
- Proper error processing.
- Pass logger into Pullers and Parsers
- Add cache for Puller services to reduce average service load
- TESTS!!!!
- Docker (Kube)
- Integration tests. Not implemented
- Docs. Refactor documentation
- Add API cache for Pullers
- Merger to Crypto App (make it better, change data structure)
- BAD bad bad price selector. Hardcoded USD currency everywhere
- Multiple Storage. Think about mering them to single package
- No bench/race tests
- No coverage report
- Code is not documented!

ISSUES:
=================
- Duplicated key value for Symbol "HOT" for price. Not solved.
- Think about Crypto storage sort and space complexity
- No requirements about Consistency, Latency, Speed, Memory, everything
- Currency code is not unique! o_O