CryptoAPI HTTP service
================================================

CryptoAPI HTTP service service is a proxy provider with data-merger and data-modifier.  

The general workflow of the service is the following:

- Pull data from the Price service
- Pull data from the Rank service
- Merge data to defined format
- Subscribe to the price and rank updates
- Periodically re-fetch data from Price/Rank services to guarantee consistency


## Installation

Use a `go-get` tool (or any go-compatible package manager) to download the app:

```
go get bitbucket.org/alexions/crypto/cmd/crypto
```

Or you can use git to clone repo manually:
```
cd $GOPATH
mkdir -p src/bitbucket.org/alexions/crypto
cd src/bitbucket.org/alexions/crypto
git clone git@bitbucket.org:alexions/crypto.git .
```


## Usage

The pullers can be run via `docker-compose`:
```
docker-compose up
```

## API

After run application the following endpoint will be available (default):

 - http://localhost:9003

The service has only onve root path: `http://localhost:9003/`

### Queries

#### limit
Service has ability to limit numbers of currencies in response:

- limit=[int] Get only [int] amount of currencies
- Example: http://localhost:9003?limit=15

#### format
You can specify response encoding: `json` or `csv`:

- format=[string] Get response in the specified format: [json, csv]
- Example: http://localhost:9003?format=csv

### CryptoCurrency response: JSON

```
[
  {
    "Symbol":"BTC",
    "Price":6708.99,
    "Rank":1
  },
  {
    "Symbol":"ETH",
    "Price":517.123,
    "Rank":2
  },
  {
    "Symbol":"LTC",
    "Price":104.439,
    "Rank":3
  },
  ...
]
```

### CryptoCurrency response: CSV
```
Symbol,Price,Rank
BTC,6717.480,1
ETH,517.737,2
LTC,104.732,3
...
```

> Hint: Check `docker-compose.yml` in case if ports already in use.

## Tests
```
go get ./...
go test ./...
```


