Puller service
================================================

Puller service is a proxy provider with data-modifier.  
It based on the perodical HTTP requests to data-source system, processing responses and building a new list of fetched data.

All puller services provide an API endpoint to grap all available data and also send updates to a PubSub-like system.  
The app architecture allows to change the PubSub system to RabbitMQ, ZeroMQ and other ones or connect gRPC or similar.

## Installation

Use a `go-get` tool (or any go-compatible package manager) to download the app:

```
go get bitbucket.org/alexions/crypto/cmd/pricePuller
// or rank puller service
go get bitbucket.org/alexions/crypto/cmd/rankPuller
```

Or you can use git to clone repo manually:
```
cd $GOPATH
mkdir -p src/bitbucket.org/alexions/crypto
cd src/bitbucket.org/alexions/crypto
git clone git@bitbucket.org:alexions/crypto.git .
```


## Usage

The pullers can be run via `docker-compose`:
```
docker-compose run pricePuller
// or 
docker-compose run rankPuller
```

## API

After run applications the following endpoints will be available (default):

 - http://localhost:9001 Rank service
 - http://localhost:9002 Price service

All services have only root path and provide a JSON-like data.

### Price reply format
```
{
  "42":{
    "symbol":"42",
    "prices":{
      "usd":23535.3
    }
  },
  "300":{
    "symbol":"300",
    "prices":{
      "usd":549.491
    }
  },
  ...
}
```

### Rank reply format
```
{
  "42":{
    "Symbol":"42",
    "SortOrder":"34"
  },
  "300":{
    "Symbol":"300",
    "SortOrder":"2212"
  },
  ...
}
```

> Hint: Check `docker-compose.yml` in case if ports already in use.

## Tests
```
go get ./...
go test ./...
```


