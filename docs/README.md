Table of Content
===================================================

1. [Read First](#read-first)
2. [General Architecture](#general-architecture)
3. [Services](#services)

Read First
===================================================

Before starting a new project I would ask a lot of questions, because the answers potentially can change the whole
project architecture. I did not do this for the test challenge by two reasons:  

1. Communication time: Discussing the questions and getting an answers usually takes too much time. Inappropriate for 
   test challenge. It's not required for the test task, but MUST BE done for a real project.
2. I used "assumptions" to simplify project architecture.

Here is the list of questions I would ask before staring working on the project:

- Path expectation and self-documentation: The question is how much paths and features do we expect from services?
  Will it be only one root path, like http://service/, or we're going to have a lot of them in the future?  
  Are we going to have sessions, user-auth, etc?
  In case of having lot of paths I would prefer to integrate https://goa.design/ framework for REST-API microservices.
  Otherwise: there is no needs to add extra code complexity to the project and we can avoid using any frameworks.

**Data pullers** (Price and Rank) (if we don't have answers, of course we need to spend some time to research
 the data-source system first):

- How often prices/ranks update?
- Which interval I should use to pull the changes?
- Do we have any limits on amount of requests?
- Consistency: What should we do if a data-source system is not available. Should we stop returning outdated results and fail
   or should we continue returning outdated results?
- How critical is the time between getting a new price/rank from data-source system and updating this data in our system?
- Do we need to provide an ability to subscribe for price/rank changes?
- Expected amount of stored data. Is it million of records or just 1k?

**Currencies API service**

- Consistency: What should we do if Price/Rank systems are not available. Should we stop returning outdated results and fail
   or should we continue returning outdated results?
- Premature optimization: What is the approximate amount of requests per second to the API endpoint?
- Do we need to provide an ability to subscribe for price/rank changes?
- How critical is the time between getting a new price/rank from data-source system and updating this data in our system?
- Availability: How critical is to reach 99.9999 for the app?
- Should we use subsciption for price/rank changes? Do we really need it? Or we can just use HTTP API.
- Expected amount of stored data. Is it million of records or just 1k?
- Merger: Is Currency Symbol consistent in both systems? (Answer: NO)

General Architecture
================================================

Talking about the challenge we don't need to build to complex system with caches, shared DB, any type of Queues, etc.  

Common HTTP API will cover all our needs! But let's asssume, that we need to get updates as soon as possible. So in this
case we need to have some PubSub (Event based/Queue) system.

![architecture](crypto.png)

As a PubSub system I decided to use Satori, because this solution does not require me to install anything locally,
because it's a CloudBased solution. Easy to use: Satori provides SDK that can be easily added to the application.

In total:

- Easy to use
- No responsibility for infra
- no local services

The architecture of all services allow to replace PubSub system to any other, like ZeroMQ, RabbitMQ, etc.

Services
================================================

The project splits into 3 services:

- Prices puller
- Ranks puller
- HTTP API service (with merger)

The puller services have the same code-base but with different initialization parameters.  
Look at [Pullers services architecture](pullers_services.md) to get more info.

Also check the [Crypto HTTP API service](crypto_service.md) documentation.