package main

import (
	"bitbucket.org/alexions/crypto/pkg/puller/app"
)

type CryptocompareJson struct {
	Data map[string]CoinData `json:"Data"`
}

type CoinData struct {
	Symbol    string `json:"Symbol"`
	SortOrder string `json:"SortOrder"`
}

type Config struct {
	app.Options
	Satori Satori
}

type Satori struct {
	AppKey   string
	Endpoint string
	Channel  string
}
