package main

import (
	"flag"
	"time"

	"bitbucket.org/alexions/crypto/pkg/pubsub/satori"
	"bitbucket.org/alexions/crypto/pkg/puller/app"
	"github.com/satori-com/satori-rtm-sdk-go/rtm"
)

func main() {
	config := getConfig()

	puller := Puller{
		Endpoint: "https://www.cryptocompare.com/api/data/coinlist/",
	}
	parser := Parser{}
	ps := satori.New(config.Satori.Endpoint, config.Satori.AppKey, rtm.Options{})

	pullApp := app.New(puller, parser, ps, config.Options)
	pullApp.PullAndServe()
}

func getConfig() Config {
	var interval int
	options := Config{}

	flag.IntVar(&interval, "interval", 30, "Pulling interval (sec)")
	options.Interval = time.Second * time.Duration(interval)
	flag.IntVar(&options.ListeningPort, "port", 9001, "API port")
	flag.StringVar(&options.Satori.AppKey, "appkey", "", "Satori Appkey")
	flag.StringVar(&options.Satori.Endpoint, "endpoint", "", "Satori endpoint")
	flag.StringVar(&options.Channel, "channel", "rank_updates", "Channel to publish changes")
	flag.Parse()

	return options
}
