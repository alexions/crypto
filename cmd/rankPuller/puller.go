package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
)

type Puller struct {
	Endpoint string
}

func (p Puller) Pull(c chan<- []byte) error {
	fmt.Println("Pulling new data from ", p.Endpoint)
	res, err := http.Get(p.Endpoint)
	if err != nil {
		return err
	}
	response, err := ioutil.ReadAll(res.Body)
	res.Body.Close()
	if err != nil {
		return err
	}
	c <- response
	return nil
}
