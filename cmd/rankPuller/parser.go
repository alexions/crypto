package main

import (
	"encoding/json"

	"bitbucket.org/alexions/crypto/pkg/puller/app"
)

type Parser struct{}

func (p Parser) Parse(data []byte, storage app.Storager) error {
	parsed := CryptocompareJson{}
	err := json.Unmarshal(data, &parsed)
	if err != nil {
		return err
	}

	for _, coin := range parsed.Data {
		if val, found := storage.Get(coin.Symbol); !found || val.(CoinData).SortOrder != coin.SortOrder {
			storage.Set(coin.Symbol, coin)
		}
	}

	return nil
}
