package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"
)

type Puller struct {
	Endpoint string
	Limit    int

	position int
}

func (p Puller) Pull(c chan<- []byte) error {
	fmt.Println("Pulling new data from ", p.Endpoint+"?limit="+strconv.Itoa(p.Limit)+"&start="+strconv.Itoa(p.position))

	res, err := http.Get(p.Endpoint + "?limit=" + strconv.Itoa(p.Limit) + "&start=" + strconv.Itoa(p.position))
	if err != nil {
		return err
	}
	response, err := ioutil.ReadAll(res.Body)
	res.Body.Close()
	if err != nil {
		return err
	}
	c <- response

	// We cannot get all coins at the same time
	// Check if we need to send one more request
	// It creates overload with parser, but we forced to check max coins amount
	parsed := CoinmarketJson{}
	err = json.Unmarshal(response, &parsed)
	if err != nil {
		return err
	}

	if p.position+p.Limit >= parsed.MetaData.NumCryptocurrencies {
		p.position = 0
	} else {
		p.position += p.Limit
		return p.Pull(c)
	}

	return nil
}
