package main

import (
	"encoding/json"

	"bitbucket.org/alexions/crypto/pkg/puller/app"
)

type Parser struct{}

func (p Parser) Parse(data []byte, storage app.Storager) error {
	parsed := CoinmarketJson{}
	err := json.Unmarshal(data, &parsed)
	if err != nil {
		return err
	}

	for _, data := range parsed.Data {
		// TODO: Do I need this assetion?
		coin := Coin{
			Symbol: data.Symbol,
			Prices: map[string]float32{
				"usd": data.Quotes["USD"].Price,
			},
		}
		// TODO: Don't like this comapator
		if val, found := storage.Get(coin.Symbol); !found || val.(Coin).Prices["usd"] != coin.Prices["usd"] {
			storage.Set(coin.Symbol, coin)
		}
	}

	return nil
}
