package main

import (
	"flag"
	"time"

	"bitbucket.org/alexions/crypto/pkg/pubsub/satori"
	"bitbucket.org/alexions/crypto/pkg/puller/app"
	"github.com/satori-com/satori-rtm-sdk-go/rtm"
)

func main() {
	puller := Puller{
		Endpoint: "https://api.coinmarketcap.com/v2/ticker/",
		Limit:    100,
	}
	parser := Parser{}

	config := getConfig()
	ps := satori.New(config.Satori.Endpoint, config.Satori.AppKey, rtm.Options{})

	pullApp := app.New(puller, parser, ps, config.Options)
	pullApp.PullAndServe()
}

func getConfig() Config {
	var interval int
	options := Config{}

	flag.IntVar(&interval, "interval", 30, "Pulling interval (sec)")
	options.Interval = time.Second * time.Duration(interval)
	flag.IntVar(&options.ListeningPort, "port", 9002, "API port")
	flag.StringVar(&options.Satori.AppKey, "appkey", "", "Satori Appkey")
	flag.StringVar(&options.Satori.Endpoint, "endpoint", "", "Satori endpoint")
	flag.StringVar(&options.Channel, "channel", "price_updates", "Channel to publish changes")
	flag.Parse()

	return options
}
