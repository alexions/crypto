package main

import "bitbucket.org/alexions/crypto/pkg/puller/app"

type CoinmarketJson struct {
	Data     map[int]CoinData `json:"data"`
	MetaData MetaData         `json:"metadata"`
}

type MetaData struct {
	Timestamp           int    `json:"timestamp"`
	NumCryptocurrencies int    `json:"num_cryptocurrencies"`
	Error               string `json:"error"`
}

type CoinData struct {
	Symbol string           `json:"symbol"`
	Quotes map[string]Quote `json:"quotes"`
}

type Quote struct {
	Price float32 `json:"price"`
}

type Coin struct {
	Symbol string             `json:"symbol"`
	Prices map[string]float32 `json:"prices"`
}

type Config struct {
	app.Options
	Satori Satori
}

type Satori struct {
	AppKey   string
	Endpoint string
	Channel  string
}
