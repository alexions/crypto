package main

import "bitbucket.org/alexions/crypto/pkg/crypto/app"

type Config struct {
	ListeningPort int
	Satori        Satori
	PriceService  app.PriceService
	RankService   app.RankService
}

type Satori struct {
	AppKey   string
	Endpoint string
}

type PullerService struct {
	Endpoint string
	Channel  string
}
