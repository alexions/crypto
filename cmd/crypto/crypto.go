package main

import (
	"flag"

	"bitbucket.org/alexions/crypto/pkg/crypto/app"
	"bitbucket.org/alexions/crypto/pkg/pubsub/satori"
	"github.com/satori-com/satori-rtm-sdk-go/rtm"
)

func main() {
	config := getConfig()
	ps := satori.New(config.Satori.Endpoint, config.Satori.AppKey, rtm.Options{})
	api := app.New(app.Options{
		ListeningPort: config.ListeningPort,
		PubSub:        ps,
		PriceService:  config.PriceService,
		RankService:   config.RankService,
	})
	api.Serve()
}

func getConfig() Config {
	config := Config{}

	flag.IntVar(&config.ListeningPort, "port", 9003, "API port")
	flag.StringVar(&config.Satori.AppKey, "appkey", "", "Satori Appkey")
	flag.StringVar(&config.Satori.Endpoint, "endpoint", "", "Satori endpoint")

	flag.StringVar(&config.PriceService.Endpoint, "price_endpoint", "http://localhost:9002", "Price service endpoint")
	flag.StringVar(&config.PriceService.Channel, "price_channel", "price_updates", "Price service channel with updated")

	flag.StringVar(&config.RankService.Endpoint, "rank_endpoint", "http://localhost:9001", "Rank service endpoint")
	flag.StringVar(&config.RankService.Channel, "rank_channel", "rank_updates", "Rank service channel with updated")

	flag.Parse()

	return config
}
